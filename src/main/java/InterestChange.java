import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * Created by qsoft on 5/17/17.
 */
public class InterestChange extends ScheduleItem
{
    public InterestChange(LocalDate dueDate, BigDecimal amount)
    {
        super(dueDate, amount);
        actualDate = dueDate;
    }

    @Override
    public BigDecimal getEndingBalance()
    {
        return beginningBalance;
    }

    @Override
    public void calculate(BusinessLoan.InterestData interestData, LocalDate now, BusinessLoan loan)
    {
        beginningBalance = interestData.balance;
        long days = ChronoUnit.DAYS.between(interestData.latestAccumulatedInterestDate, dueDate) - 1; //not include start date
        BigDecimal interest = beginningBalance.multiply(BigDecimal.valueOf(days * interestData.dailyRate));

        interestData.dailyRate = loan.getDailyRate(amount);
        interestData.accumulatedInterest = interestData.accumulatedInterest.add(interest);
        interestData.latestAccumulatedInterestDate = dueDate.minusDays(1);
    }
    public String toString()
    {
        return super.toString() + "\n";
    }
}
