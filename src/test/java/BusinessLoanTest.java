import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static org.junit.Assert.*;

public class BusinessLoanTest
{
    BusinessLoan loan;
    Repayment jul01, aug01, sep01, oct01, nov01;

    @Before
    public void setup()
    {
        BusinessLoan.SCALE = 2;
        loan = new BusinessLoan(LocalDate.of(2016, Month.JUNE, 1), LocalDate.of(2016, Month.JULY, 1),
                BigDecimal.valueOf(225000), BusinessLoan.RepaymentType.PpI, 5, BigDecimal.valueOf(36.5));
        jul01 = (Repayment)loan.scheduleItems.get(0);
        aug01 = (Repayment)loan.scheduleItems.get(1);
        sep01 = (Repayment)loan.scheduleItems.get(2);
        oct01 = (Repayment)loan.scheduleItems.get(3);
        nov01 = (Repayment)loan.scheduleItems.get(4);
    }

    @Test
    public void testGenerateSchedule_PpI()
    {
        List<ScheduleItem> payments = loan.scheduleItems;
        payments.forEach(p -> System.err.println(p));
        assertEquals(5, payments.size());

        assertEquals(6975, jul01.interest.doubleValue(), 0.0001);
        assertEquals(5580, aug01.interest.doubleValue(), 0.0001);
        assertEquals(4185, sep01.interest.doubleValue(), 0.0001);
        assertEquals(2700, oct01.interest.doubleValue(), 0.0001);
        assertEquals(1395, nov01.interest.doubleValue(), 0.0001);
    }

    @Test
    public void testPayMoreThanScheduled_lastPrincipalShouldDecrease()
    {
        jul01.pay();
        loan.payAfterRecalculatingInterest(aug01, BigDecimal.valueOf(60000).add(aug01.interest));

        assertEquals("aug01 principal", 60000, aug01.principal.doubleValue(), 0.0001);

        assertEquals("same", 6975, jul01.interest.doubleValue(), 0.0001);
        assertEquals("same", 5580, aug01.interest.doubleValue(), 0.0001);
        assertEquals("decrease", 3720, sep01.interest.doubleValue(), 0.0001);
        assertEquals("oct01", 2250, oct01.interest.doubleValue(), 0.0001);
        assertEquals("nov01", 930, nov01.interest.doubleValue(), 0.0001);

        assertEquals("nov01 principal decrease", 30000, nov01.principal.doubleValue(), 0.0001);

        System.err.println(loan);
    }

    @Test
    public void testPayLessThanScheduled_lastPrincipalShouldIncreaseIfNotReachFixedAmount()
    {
        loan.payAfterRecalculatingInterest(jul01, BigDecimal.valueOf(60000).add(jul01.interest));
        assertEquals("jul01 principal", 60000, jul01.principal.doubleValue(), 0.0001);
        assertEquals("nov01 principal decrease", 30000, nov01.principal.doubleValue(), 0.0001);
        System.err.println(loan);

        loan.payAfterRecalculatingInterest(aug01, BigDecimal.valueOf(35000).add(aug01.interest));
        assertEquals("nov01 principal increase", 40000, nov01.principal.doubleValue(), 0.0001);
        System.err.println(loan);
    }

    @Test
    public void testPayLessThanInterest_ShouldAddMorePaymentsAtTheEnd()
    {
        jul01.pay();
        System.err.println(loan);

        loan.payAfterRecalculatingInterest(aug01, BigDecimal.valueOf(2000));

        System.err.println(loan);

        assertEquals("aug01 principal", -3580, aug01.principal.doubleValue(), 0.0001);

        assertEquals("two more payments added",7, loan.scheduleItems.size());
        assertEquals("same", 6975, jul01.interest.doubleValue(), 0.0001);
        assertEquals("same", 5580, aug01.interest.doubleValue(), 0.0001);
        assertEquals("decrease", 5690.98, sep01.interest.doubleValue(), 0.0001);
        assertEquals("oct01", 4157.4, oct01.interest.doubleValue(), 0.0001);
        assertEquals("nov01", 2900.98, nov01.interest.doubleValue(), 0.0001);

        Repayment dec01 = (Repayment)loan.scheduleItems.get(5);
        Repayment jan01 = (Repayment)loan.scheduleItems.get(6);
        assertEquals("dec01", 1457.4, dec01.interest.doubleValue(), 0.0001);
        assertEquals("dec01", 45000, dec01.principal.doubleValue(), 0.0001);
        assertEquals("jan01", 110.98, jan01.interest.doubleValue(), 0.0001);
        assertEquals("jan01", 3580, jan01.principal.doubleValue(), 0.0001);
    }

    @Test
    public void testAddFunding_ShouldIncreaseInterestAndAddMorePaymentsAtTheEnd()
    {
        jul01.pay();
        aug01.pay();
        LocalDate now = LocalDate.of(2016, Month.AUGUST, 16);
        Funding funding = loan.addFunding(LocalDate.of(2016, Month.AUGUST, 15), BigDecimal.valueOf(50000), true, now);

        assertEquals(funding, loan.scheduleItems.get(2));
        assertEquals("funding principal", 50000, funding.amount.doubleValue(), 0.0001);

        assertEquals("jul01 - same", 6975, jul01.interest.doubleValue(), 0.0001);
        assertEquals("aug01 - same", 5580, aug01.interest.doubleValue(), 0.0001);
        assertEquals("sep01 - incr\n" + loan, 5085, sep01.interest.doubleValue(), 0.0001);
        assertEquals("oct01", 4200, oct01.interest.doubleValue(), 0.0001);
        assertEquals("nov01", 2945, nov01.interest.doubleValue(), 0.0001);
        assertEquals("nov01 principal increase", 45000, nov01.principal.doubleValue(), 0.0001);
        assertEquals("3 added", 8, loan.scheduleItems.size());
        Repayment dec01 = (Repayment)loan.scheduleItems.get(6);
        Repayment jan01 = (Repayment)loan.scheduleItems.get(7);
        assertEquals("dec01", 1500, dec01.interest.doubleValue(), 0.0001);
        assertEquals("jan01", 155, jan01.interest.doubleValue(), 0.0001);
        assertEquals("dec01", 45000, dec01.principal.doubleValue(), 0.0001);
        assertEquals("jan01", 5000, jan01.principal.doubleValue(), 0.0001);
    }

    @Test
    public void testAddBalloonPayment()
    {
        assertEquals(5, loan.scheduleItems.size());

        LocalDate aug16 = LocalDate.of(2016, Month.AUGUST, 16);
        Repayment repayment = loan.addUnpaidRepayment(aug16, BigDecimal.valueOf(100000), loan.firstPaymentDate);
        assertEquals(repayment, loan.scheduleItems.get(2));
        assertEquals(100000, repayment.amount.doubleValue(), 0.001);
        System.err.println(loan);
        assertEquals(loan.toString(), 5+1-2, loan.scheduleItems.size());
    }

    @Test
    public void testUpdateFunding_ShouldRestructureAndCalculateInterest()
    {
        assertEquals(5, loan.scheduleItems.size());

        LocalDate now = LocalDate.of(2016, Month.AUGUST, 16);
        Funding funding = loan.addFunding(LocalDate.of(2016, Month.AUGUST, 15), BigDecimal.valueOf(50000), false, now);
        assertEquals(funding, loan.scheduleItems.get(2));
        assertEquals(loan.toString(), 5+1+2, loan.scheduleItems.size());

        funding.dueDate = oct01.dueDate;
        loan.reconstructSchedule();
        assertEquals(funding, loan.scheduleItems.get(3));

        funding.amount = BigDecimal.valueOf(150000);
        loan.reconstructSchedule(loan.firstFundingDate, false);
        assertEquals(loan.toString(), 5+1+4, loan.scheduleItems.size());
        assertEquals(funding, loan.scheduleItems.get(3));
        System.err.println(loan);
    }

    @Test
    public void testAddFunding_TwoInARow()
    {
        jul01.pay();
        aug01.pay();
        LocalDate now = LocalDate.of(2016, Month.AUGUST, 21);
        Funding funding1 = loan.addFunding(LocalDate.of(2016, Month.AUGUST, 15), BigDecimal.valueOf(20000), true, now);
        Funding funding2 = loan.addFunding(LocalDate.of(2016, Month.AUGUST, 20), BigDecimal.valueOf(30000), true, now);

        assertEquals(funding1, loan.scheduleItems.get(2));
        assertEquals(funding2, loan.scheduleItems.get(3));

        loan.scheduleItems.forEach(p -> System.err.println(p));

        assertEquals("jul01 - same", 6975, jul01.interest.doubleValue(), 0.0001);
        assertEquals("aug01 - same", 5580, aug01.interest.doubleValue(), 0.0001);
        assertEquals("sep01 - incr", 4935, sep01.interest.doubleValue(), 0.0001);
        assertEquals("oct01", 4200, oct01.interest.doubleValue(), 0.0001);
        assertEquals("nov01", 2945, nov01.interest.doubleValue(), 0.0001);
        assertEquals("nov01 principal increase", 45000, nov01.principal.doubleValue(), 0.0001);
        assertEquals("4 added", 9, loan.scheduleItems.size());
        Repayment dec01 = (Repayment)loan.scheduleItems.get(7);
        Repayment jan01 = (Repayment)loan.scheduleItems.get(8);
        assertEquals("dec01", 1500, dec01.interest.doubleValue(), 0.0001);
        assertEquals("dec01", 45000, dec01.principal.doubleValue(), 0.0001);
        assertEquals("jan01", 155, jan01.interest.doubleValue(), 0.0001);
        assertEquals("jan01", 5000, jan01.principal.doubleValue(), 0.0001);
    }

    @Test
    public void testChangeInterest_NoOverDuePayment()
    {
        LocalDate now = loan.firstFundingDate;
        loan.addInterestChange(LocalDate.of(2016, Month.AUGUST, 15), BigDecimal.valueOf(36.5 * 1.5), now);
        loan.scheduleItems.forEach(p -> System.err.println(p));

        assertEquals(6, loan.scheduleItems.size());
        InterestChange interestChange = (InterestChange) loan.scheduleItems.get(2);
        assertEquals(interestChange.beginningBalance.doubleValue(), aug01.getEndingBalance().doubleValue(), 0.001);
        assertEquals(interestChange.beginningBalance.doubleValue(), interestChange.getEndingBalance().doubleValue(), 0.001);

        assertEquals(6975, jul01.interest.doubleValue(), 0.0001);
        assertEquals(5580, aug01.interest.doubleValue(), 0.0001);
        assertEquals(5400, sep01.interest.doubleValue(), 0.0001);
        assertEquals(4050, oct01.interest.doubleValue(), 0.0001);
        assertEquals(2092.5, nov01.interest.doubleValue(), 0.0001);
    }


    @Test
    public void testPnI_recalculateRepayments()
    //PnI Case1
    {
        loan = new BusinessLoan(LocalDate.of(2017, Month.MAY, 21),
                LocalDate.of(2017, Month.JUNE, 21),
                BigDecimal.valueOf(1000000), BusinessLoan.RepaymentType.PnI, 4, BigDecimal.valueOf(12));
        assertEquals(256281.09, loan.fixedAmount.doubleValue(), 0.001);

        assertEquals(4, loan.scheduleItems.size());
        Repayment jun = (Repayment) loan.scheduleItems.get(0);
        Repayment jul = (Repayment) loan.scheduleItems.get(1);
        Repayment aug = (Repayment) loan.scheduleItems.get(2);
        Repayment sep = (Repayment) loan.scheduleItems.get(3);
        assertEqualsRepayment(jun, LocalDate.of(2017, Month.JUNE, 21), 256281.09, 245760.54, 10520.55);
        assertEqualsRepayment(jul, LocalDate.of(2017, Month.JULY, 21), 256281.09, 248842.02, 7439.07);
        assertEqualsRepayment(aug, LocalDate.of(2017, Month.AUGUST, 21), 256281.09, 251130.19, 5150.90);
        assertEqualsRepayment(sep, LocalDate.of(2017, Month.SEPTEMBER, 21), 256858.69, 254267.25, 2591.44);

        System.err.println(loan);
        loan.payAfterRecalculatingInterest(jun, jun.dueDate.plusDays(1), jun.dueDate);
        System.err.println(loan);


        Repayment added = (Repayment) loan.scheduleItems.get(4);
        assertEquals(0, added.getEndingBalance().doubleValue(), 0.01);
    }

    @Test
    public void testPnI_recalculateRepayments_PaidBeforeDueDate_NextRepaymentInterestShouldIncrease()
    {
        BusinessLoan.SCALE = 0;
        loan = new BusinessLoan(LocalDate.of(2019, Month.JULY, 19),
                LocalDate.of(2019, Month.SEPTEMBER, 19),
                BigDecimal.valueOf(1000000), BusinessLoan.RepaymentType.PnI, 3, BigDecimal.valueOf(10));
        System.err.println(loan);

        Repayment first = (Repayment) loan.scheduleItems.get(0);
        Repayment second = (Repayment) loan.scheduleItems.get(2);
        BigDecimal oldSecondInterest = second.interest;

        first.interest = BigDecimal.valueOf(17260);
        first.principal = loan.fixedAmount.subtract(first.interest);
        first.pay(LocalDate.of(2019, Month.JULY, 19));
        loan.reconstructSchedule(first.actualDate, false);

        System.err.println(loan);
        assertEquals(-1, oldSecondInterest.compareTo(second.interest));
        assertEquals(4, loan.scheduleItems.size());
        assertEquals(first.dueDate.plusMonths(3), loan.scheduleItems.get(3).dueDate);
    }

    @Test
    public void testPnI_recalculateRepayments_OneTerm_PayNotEnough()
    {
        loan = new BusinessLoan(LocalDate.of(2017, Month.MAY, 21),
                LocalDate.of(2017, Month.JUNE, 21),
                BigDecimal.valueOf(100000), BusinessLoan.RepaymentType.PnI, 1, BigDecimal.valueOf(12));

        assertEquals(1, loan.scheduleItems.size());
        Repayment jun = (Repayment) loan.scheduleItems.get(0);
        System.err.println(loan);
        assertEqualsRepayment(jun, LocalDate.of(2017, Month.JUNE, 21), 101052.05, 100000, 1052.05);

        loan.payAfterRecalculatingInterest(jun, BigDecimal.valueOf(5000));
        System.err.println(loan);
        assertEquals(2, loan.scheduleItems.size());

        Repayment added = (Repayment) loan.scheduleItems.get(1);
        assertEquals(0, added.getEndingBalance().doubleValue(), 0.01);
        assertEquals(jun.dueDate.plusMonths(1), added.dueDate);
    }

    @Test
    public void testPpI_recalculateRepayments_OneTerm_PayNotEnough()
    {
        loan = new BusinessLoan(LocalDate.of(2017, Month.MAY, 21),
                LocalDate.of(2017, Month.JUNE, 21),
                BigDecimal.valueOf(100000), BusinessLoan.RepaymentType.PpI, 1, BigDecimal.valueOf(12));

        assertEquals(1, loan.scheduleItems.size());
        Repayment jun = (Repayment) loan.scheduleItems.get(0);
        System.err.println(loan);
        assertEqualsRepayment(jun, LocalDate.of(2017, Month.JUNE, 21), 101052.05, 100000, 1052.05);

        loan.payAfterRecalculatingInterest(jun, BigDecimal.valueOf(5000));
        System.err.println(loan);
        assertEquals(2, loan.scheduleItems.size());

        Repayment added = (Repayment) loan.scheduleItems.get(1);
        assertEquals(0, added.getEndingBalance().doubleValue(), 0.01);
        assertEquals(jun.dueDate.plusMonths(1), added.dueDate);
    }

    @Test
    public void testInt_recalculateRepayments_OneTerm_PayNotEnough()
    {
        loan = new BusinessLoan(LocalDate.of(2017, Month.MAY, 21),
                LocalDate.of(2017, Month.JUNE, 21),
                BigDecimal.valueOf(100000), BusinessLoan.RepaymentType.Int, 1, BigDecimal.valueOf(12));

        assertEquals(1, loan.scheduleItems.size());
        Repayment jun = (Repayment) loan.scheduleItems.get(0);
        System.err.println(loan);
        assertEqualsRepayment(jun, LocalDate.of(2017, Month.JUNE, 21), 101052.05, 100000, 1052.05);

        loan.payAfterRecalculatingInterest(jun, BigDecimal.valueOf(5000));
        System.err.println(loan);
        assertEquals(2, loan.scheduleItems.size());

        Repayment added = (Repayment) loan.scheduleItems.get(1);
        assertEquals(0, added.getEndingBalance().doubleValue(), 0.01);
        assertEquals(jun.dueDate.plusMonths(1), added.dueDate);
    }

    @Test
    public void testPnI_recalculateRepayments_lastPaymentIncreasesAndDecreases()
    {
        loan = new BusinessLoan(LocalDate.of(2017, Month.MAY, 21),
                LocalDate.of(2017, Month.JUNE, 21),
                BigDecimal.valueOf(1000000), BusinessLoan.RepaymentType.PnI, 4, BigDecimal.valueOf(12));
        assertEquals(256281.09, loan.fixedAmount.doubleValue(), 0.001);
        System.err.println(loan);

        assertEquals(4, loan.scheduleItems.size());
        Repayment jun = (Repayment) loan.scheduleItems.get(0);
        Repayment jul = (Repayment) loan.scheduleItems.get(1);
        Repayment aug = (Repayment) loan.scheduleItems.get(2);
        Repayment sep = (Repayment) loan.scheduleItems.get(3);
        assertEquals("last repayment greater than fixed amount", 1, sep.amount.compareTo(loan.fixedAmount));

        loan.payAfterRecalculatingInterest(jun, BigDecimal.valueOf(300000).add(jun.interest));
        System.err.println(loan);
        assertEquals(4, loan.scheduleItems.size());
        assertEquals("last repayment decreases, greater than fixed amount", -1, sep.amount.compareTo(loan.fixedAmount));
        BigDecimal last = sep.amount;

        loan.payAfterRecalculatingInterest(jul, BigDecimal.valueOf(200000).add(jul.interest));
        System.err.println(loan);
        assertEquals(4, loan.scheduleItems.size());
        assertEquals("last repayment increase a bit", 1, sep.amount.compareTo(last));
        assertEquals("last repayment still less than fixed amount", -1, sep.amount.compareTo(loan.fixedAmount));
    }

    @Test
    public void testPnI_recalculateRepayments_lastPaymentDeletedAndAdded()
    {
        loan = new BusinessLoan(LocalDate.of(2017, Month.MAY, 21),
                LocalDate.of(2017, Month.JUNE, 21),
                BigDecimal.valueOf(1000000), BusinessLoan.RepaymentType.PnI, 4, BigDecimal.valueOf(12));
        assertEquals(256281.09, loan.fixedAmount.doubleValue(), 0.001);
        System.err.println(loan);

        assertEquals(4, loan.scheduleItems.size());
        Repayment jun = (Repayment) loan.scheduleItems.get(0);
        Repayment jul = (Repayment) loan.scheduleItems.get(1);
        Repayment aug = (Repayment) loan.scheduleItems.get(2);
        Repayment sep = (Repayment) loan.scheduleItems.get(3);

        loan.payAfterRecalculatingInterest(jun, BigDecimal.valueOf(500000).add(jun.interest));
        System.err.println(loan);
        assertEquals(3, loan.scheduleItems.size());
        assertEquals("should not remove deleted repayments", 4, loan.repayments.size());
        assertTrue(sep.deleted);

        loan.payAfterRecalculatingInterest(jul, BigDecimal.valueOf(100000).add(jul.interest));
        System.err.println(loan);
        assertEquals(4, loan.scheduleItems.size());
        assertTrue(sep.deleted);
        assertFalse(loan.scheduleItems.get(3).deleted);
        assertEquals("should not remove deleted repayments", 5, loan.repayments.size());
    }

    @Test
    public void testPnI_recalculateRepayments_lastScheduleItemIsNotRepayment()
    {
        loan = new BusinessLoan(LocalDate.of(2017, Month.MAY, 21),
                LocalDate.of(2017, Month.JUNE, 21),
                BigDecimal.valueOf(1000000), BusinessLoan.RepaymentType.PnI, 4, BigDecimal.valueOf(12));
        assertEquals(256281.09, loan.fixedAmount.doubleValue(), 0.001);

        assertEquals(4, loan.scheduleItems.size());
        Repayment jun = (Repayment) loan.scheduleItems.get(0);
        Repayment jul = (Repayment) loan.scheduleItems.get(1);
        Repayment aug = (Repayment) loan.scheduleItems.get(2);
        Repayment sep = (Repayment) loan.scheduleItems.get(3);

        loan.addInterestChange(sep.dueDate.plusDays(1), BigDecimal.valueOf(10), loan.firstFundingDate);
        System.err.println(loan);
        assertEquals(5, loan.scheduleItems.size());
        InterestChange interestChange = (InterestChange) loan.scheduleItems.get(4);

        loan.payAfterRecalculatingInterest(jun, BigDecimal.valueOf(500000).add(jun.interest));
        System.err.println(loan);
        assertFalse(interestChange.deleted);
        assertEquals(4, loan.scheduleItems.size());
        assertEquals("should not remove deleted repayments", 4, loan.repayments.size());
        assertTrue(sep.deleted);

        loan.payAfterRecalculatingInterest(jul, BigDecimal.valueOf(100000).add(jul.interest));
        System.err.println(loan);
        assertEquals(5, loan.scheduleItems.size());
        assertTrue(sep.deleted);
        assertEquals(InterestChange.class, loan.scheduleItems.get(3).getClass());
        Repayment newSep = (Repayment) loan.scheduleItems.get(4);
        assertFalse(newSep.deleted);
        assertEquals(sep.dueDate, newSep.dueDate);
        assertEquals("should not remove deleted repayments", 5, loan.repayments.size());
    }

    @Test
    public void testSchedule_PnI()
    {
        loan = new BusinessLoan(LocalDate.of(2019, Month.MAY, 21),
                LocalDate.of(2019, Month.JUNE, 21),
                BigDecimal.valueOf(1000000), BusinessLoan.RepaymentType.PnI, 4, BigDecimal.valueOf(12));

        assertEquals(256281.09, loan.fixedAmount.doubleValue(), 0.01);

        Repayment jun = new Repayment(loan.firstPaymentDate);
        Repayment jul = new Repayment(LocalDate.of(2019, Month.JULY, 21));
        Repayment aug = new Repayment(LocalDate.of(2019, Month.AUGUST, 21));
        Repayment sep = new Repayment(LocalDate.of(2019, Month.SEPTEMBER, 21));
        jun.setAmounts(256281.09, 245760.54 , 10520.55 , 754239.46 );
        jul.setAmounts(256281.09, 248842.02 , 7439.07 , 505397.44 );
        aug.setAmounts(256281.09, 251130.19 , 5150.90 , 254267.25 );
        sep.setAmounts(256858.69, 254267.25 , 2591.44 , 0);

        System.err.println(loan);

        checkIfEquals(jun, loan.scheduleItems.get(0));
        checkIfEquals(jul, loan.scheduleItems.get(1));
        checkIfEquals(aug, loan.scheduleItems.get(2));
        checkIfEquals(sep, loan.scheduleItems.get(3));
    }

    @Test
    public void testReconstructSchedule_LongTerms() {
        int terms = 600;
        loan = new BusinessLoan(LocalDate.of(2019, Month.MAY, 21),
                LocalDate.of(2019, Month.JUNE, 21),
                BigDecimal.valueOf(1000000), BusinessLoan.RepaymentType.PpI, terms, BigDecimal.valueOf(12));
        List<Repayment> repayments = new ArrayList<>();
        while (!loan.repayments.isEmpty()) {
            int i = (int)Math.random() % loan.repayments.size();
            repayments.add(loan.repayments.get(i));
            loan.repayments.remove(i);
        }
        loan.repayments = repayments;
        long s = System.currentTimeMillis();
        loan.reconstructSchedule();
        long e = System.currentTimeMillis();
        assertTrue("Restructuring takes too long (ms): " + (e-s), 10 > e - s);
    }

    @Test
    public void testReconstructSchedule_FirstFundingDateAsNow_ShouldReorderByDateAndType()
    {
        loan = new BusinessLoan(LocalDate.of(2019, Month.MAY, 21),
                LocalDate.of(2019, Month.JUNE, 21),
                BigDecimal.valueOf(1000000), BusinessLoan.RepaymentType.PpI, 4, BigDecimal.valueOf(12));

        assertEquals(250000, loan.fixedAmount.doubleValue(), 0.01);

        //repayments only
        Repayment jun = new Repayment(loan.firstPaymentDate);
        Repayment jul = new Repayment(LocalDate.of(2019, Month.JULY, 21));
        Repayment aug = new Repayment(LocalDate.of(2019, Month.AUGUST, 21));
        Repayment sep = new Repayment(LocalDate.of(2019, Month.SEPTEMBER, 21));
        loan.repayments = new ArrayList<>(Arrays.asList(aug, sep, jun, jul));
        loan.repayments.forEach(payment -> payment.principal = loan.fixedAmount);

        loan.reconstructSchedule();

        System.err.println(loan);
        assertEquals(4, loan.scheduleItems.size());
        assertEquals(jun, loan.scheduleItems.get(0));
        assertEquals(jul, loan.scheduleItems.get(1));
        assertEquals(aug, loan.scheduleItems.get(2));
        assertEquals(sep, loan.scheduleItems.get(3));
        assertEquals(0, sep.getEndingBalance().doubleValue(), 0.0001);

        //repayments and fundings
        Funding julyFunding = new Funding(LocalDate.of(2019, Month.JULY, 31), BigDecimal.valueOf(100000));
        Funding augustFunding = new Funding(aug.dueDate, BigDecimal.valueOf(200000));
        loan.fundings = Arrays.asList(augustFunding, julyFunding);

        loan.reconstructSchedule();

        System.err.println(loan);

        assertEquals(jun, loan.scheduleItems.get(0));
        assertEquals(jul, loan.scheduleItems.get(1));
        assertEquals(julyFunding, loan.scheduleItems.get(2));
        assertEquals("funding should before repayment of the same date", augustFunding, loan.scheduleItems.get(3));
        assertEquals(aug, loan.scheduleItems.get(4));
        assertEquals(sep, loan.scheduleItems.get(5));
        Repayment added1 = (Repayment) loan.scheduleItems.get(6);
        assertEquals(250000, added1.principal.doubleValue(), 0.001);
        assertEquals(sep.dueDate.plusMonths(1), added1.dueDate);
        Repayment added2 = (Repayment) loan.scheduleItems.get(7);
        assertEquals(50000, added2.principal.doubleValue(), 0.001);
        assertEquals(sep.dueDate.plusMonths(2), added2.dueDate);
        assertEquals(0, added2.getEndingBalance().doubleValue(), 0.0001);

        assertEquals(8, loan.scheduleItems.size());

        //interest change
        loan.interestMap.put(augustFunding.dueDate, BigDecimal.valueOf(10));
        loan.interestMap.put(sep.dueDate, BigDecimal.valueOf(5));

        loan.reconstructSchedule();

        System.err.println(loan);
        assertEquals(jun, loan.scheduleItems.get(0));
        assertEquals(jul, loan.scheduleItems.get(1));
        assertEquals(julyFunding, loan.scheduleItems.get(2));
        InterestChange augChange = (InterestChange) loan.scheduleItems.get(3);
        assertEquals("interest change should before funding of the same date", 10, augChange.amount.doubleValue(), 0.01);
        assertEquals(augustFunding, loan.scheduleItems.get(4));
        assertEquals(aug, loan.scheduleItems.get(5));
        InterestChange sepChange = (InterestChange) loan.scheduleItems.get(6);
        assertEquals("interest change should before funding of the same date", 5, sepChange.amount.doubleValue(), 0.01);
        assertEquals(sep, loan.scheduleItems.get(7));
        added1 = (Repayment) loan.scheduleItems.get(8);
        assertEquals(250000, added1.principal.doubleValue(), 0.001);
        assertEquals(sep.dueDate.plusMonths(1), added1.dueDate);
        added2 = (Repayment) loan.scheduleItems.get(9);
        assertEquals(50000, added2.principal.doubleValue(), 0.001);
        assertEquals(sep.dueDate.plusMonths(2), added2.dueDate);
        assertEquals(0, added2.getEndingBalance().doubleValue(), 0.0001);
        assertEquals(10, loan.scheduleItems.size());
    }

    @Test
    public void testReconstructSchedule_PpI_WithOverdueLessThanOneMonth_ShouldRecalculateInterest()
    //Scenario 1.a https://docs.google.com/spreadsheets/d/1-dVOKy6ehnc77rw-CYHDPBHGHbD0WwrMDHZ4wPXZL40/edit#gid=0
    {
        loan = new BusinessLoan(LocalDate.of(2017, Month.JUNE, 1),
                LocalDate.of(2017, Month.JULY, 1),
                BigDecimal.valueOf(225000), BusinessLoan.RepaymentType.PpI, 5, BigDecimal.valueOf(36.5));

        assertEquals(45000, loan.fixedAmount.doubleValue(), 0.01);
        System.err.println(loan);
        assertEquals(5, loan.scheduleItems.size());
        Repayment jul = (Repayment) loan.scheduleItems.get(0);
        Repayment aug = (Repayment) loan.scheduleItems.get(1);
        Repayment sep = (Repayment) loan.scheduleItems.get(2);
        Repayment oct = (Repayment) loan.scheduleItems.get(3);
        Repayment nov = (Repayment) loan.scheduleItems.get(4);
        assertEqualsRepayment(jul, LocalDate.of(2017, Month.JULY, 1), 51975, 45000, 6975);
        assertEqualsRepayment(aug, LocalDate.of(2017, Month.AUGUST, 1), 50580, 45000, 5580);
        assertEqualsRepayment(sep, LocalDate.of(2017, Month.SEPTEMBER, 1), 49185, 45000, 4185);
        assertEqualsRepayment(oct, LocalDate.of(2017, Month.OCTOBER, 1), 47700, 45000, 2700);
        assertEqualsRepayment(nov, LocalDate.of(2017, Month.NOVEMBER, 1), 46395, 45000, 1395);

        //view on first payment date -> no change to interests
        loan.reconstructSchedule(jul.dueDate, false);
        assertEqualsRepayment(jul, LocalDate.of(2017, Month.JULY, 1), 51975, 45000, 6975);

        //payAfterRecalculatingInterest first repayment on due date -> no change
        loan.payWithoutRecalculatingInterest(jul, jul.dueDate, jul.dueDate);
        assertEqualsRepayment(jul, LocalDate.of(2017, Month.JULY, 1), 51975, 45000, 6975);//same as before
        assertEqualsRepayment(aug, LocalDate.of(2017, Month.AUGUST, 1), 50580, 45000, 5580);//same as before

        //view on Aug16, aug not yet paid -> aug overdue, aug interest increases, sep interest descreases.
        loan.reconstructSchedule(aug.dueDate.plusDays(15), false);
        assertEqualsRepayment(jul, LocalDate.of(2017, Month.JULY, 1), 51975, 45000, 6975); //same
        assertEqualsRepayment(aug, LocalDate.of(2017, Month.AUGUST, 1), 53280, 45000, 8280); //increase
        assertEqualsRepayment(sep, LocalDate.of(2017, Month.SEPTEMBER, 1), 47160, 45000, 2160); //decrease
        assertEqualsRepayment(oct, LocalDate.of(2017, Month.OCTOBER, 1), 47700, 45000, 2700); // same

        //payAfterRecalculatingInterest aug 15 days late
        loan.payAfterRecalculatingInterest(aug, aug.dueDate.plusDays(15), aug.dueDate.plusDays(15));
        assertEqualsRepayment(jul, LocalDate.of(2017, Month.JULY, 1), 51975, 45000, 6975); //same
        assertEqualsRepayment(aug, LocalDate.of(2017, Month.AUGUST, 1), 53280, 45000, 8280); //increase
        assertEqualsRepayment(sep, LocalDate.of(2017, Month.SEPTEMBER, 1), 47160, 45000, 2160); //decrease
        assertEqualsRepayment(oct, LocalDate.of(2017, Month.OCTOBER, 1), 47700, 45000, 2700); // same

        //view on Sep11, sep not yet paid -> sep overdue, sep interest increases, oct interest descreases.
        loan.reconstructSchedule(sep.dueDate.plusDays(10), false);
        assertEqualsRepayment(aug, LocalDate.of(2017, Month.AUGUST, 1), 53280, 45000, 8280); //same
        assertEqualsRepayment(sep, LocalDate.of(2017, Month.SEPTEMBER, 1), 48510, 45000, 3510); //increase
        assertEqualsRepayment(oct, LocalDate.of(2017, Month.OCTOBER, 1), 46800, 45000, 1800); // decrease
        assertEqualsRepayment(nov, LocalDate.of(2017, Month.NOVEMBER, 1), 46395, 45000, 1395); //same
    }

    @Test
    public void testReconstructSchedule_PpI_WithOverdueMoreThanOneMonth_ShouldRecalculateInterest()
    //Scenario 1.b https://docs.google.com/spreadsheets/d/1-dVOKy6ehnc77rw-CYHDPBHGHbD0WwrMDHZ4wPXZL40/edit#gid=0
    {
        loan = new BusinessLoan(LocalDate.of(2017, Month.JUNE, 1),
                LocalDate.of(2017, Month.JULY, 1),
                BigDecimal.valueOf(225000), BusinessLoan.RepaymentType.PpI, 5, BigDecimal.valueOf(36.5));

        assertEquals(45000, loan.fixedAmount.doubleValue(), 0.01);
        System.err.println(loan);
        assertEquals(5, loan.scheduleItems.size());
        Repayment jul = (Repayment) loan.scheduleItems.get(0);
        Repayment aug = (Repayment) loan.scheduleItems.get(1);
        Repayment sep = (Repayment) loan.scheduleItems.get(2);
        Repayment oct = (Repayment) loan.scheduleItems.get(3);
        Repayment nov = (Repayment) loan.scheduleItems.get(4);
        assertEqualsRepayment(jul, LocalDate.of(2017, Month.JULY, 1), 51975, 45000, 6975);
        assertEqualsRepayment(aug, LocalDate.of(2017, Month.AUGUST, 1), 50580, 45000, 5580);
        assertEqualsRepayment(sep, LocalDate.of(2017, Month.SEPTEMBER, 1), 49185, 45000, 4185);
        assertEqualsRepayment(oct, LocalDate.of(2017, Month.OCTOBER, 1), 47700, 45000, 2700);
        assertEqualsRepayment(nov, LocalDate.of(2017, Month.NOVEMBER, 1), 46395, 45000, 1395);

        //payAfterRecalculatingInterest first repayment on due date -> no change to aug
        loan.payAfterRecalculatingInterest(jul, jul.dueDate, jul.dueDate);
        System.err.println(loan);
        assertEqualsRepayment(aug, LocalDate.of(2017, Month.AUGUST, 1), 50580, 45000, 5580);

        //payAfterRecalculatingInterest aug on Oct16 -> aug, sep, oct overdue
        LocalDate now = oct.dueDate.plusDays(15);
        loan.reconstructSchedule(now,false);
        loan.payAfterRecalculatingInterest(aug, now, now);
        assertEqualsRepayment(aug, LocalDate.of(2017, Month.AUGUST, 1), 64260, 45000, 19260); //increase

        //view on Oct20, sep still unpaid
        loan.reconstructSchedule(oct.dueDate.plusDays(19), false);
        assertEqualsRepayment(jul, LocalDate.of(2017, Month.JULY, 1), 51975, 45000, 6975); //same
        assertEqualsRepayment(aug, LocalDate.of(2017, Month.AUGUST, 1), 64260, 45000, 19260); //increase
        assertEqualsRepayment(sep, LocalDate.of(2017, Month.SEPTEMBER, 1), 45540, 45000, 540); //decrease
        assertEqualsRepayment(oct, LocalDate.of(2017, Month.OCTOBER, 1), 45000, 45000, 0); // recalculated
        assertEqualsRepayment(nov, LocalDate.of(2017, Month.NOVEMBER, 1), 45540, 45000, 540); //recalculated
    }

    private void assertEqualsRepayment(ScheduleItem item, LocalDate dueDate, double payment, double principal, double interest)
    {
        assertEquals(item.getClass(), Repayment.class);
        Repayment repayment = (Repayment)item;
        assertEquals(dueDate, repayment.dueDate);
        assertEquals(payment, repayment.amount.doubleValue(), 0.0001);
        assertEquals(principal, repayment.principal.doubleValue(), 0.0001);
        assertEquals(interest, repayment.interest.doubleValue(), 0.0001);
    }


    @Test
    public void testGenerateSchedule_Int()
    {
        loan = new BusinessLoan(LocalDate.of(2019, Month.MAY, 21),
                LocalDate.of(2019, Month.JUNE, 21),
                BigDecimal.valueOf(1000000), BusinessLoan.RepaymentType.Int, 4, BigDecimal.valueOf(12));

        Repayment jun = new Repayment(loan.firstPaymentDate);
        Repayment jul = new Repayment(LocalDate.of(2019, Month.JULY, 21));
        Repayment aug = new Repayment(LocalDate.of(2019, Month.AUGUST, 21));
        Repayment sep = new Repayment(LocalDate.of(2019, Month.SEPTEMBER, 21));
        jun.setAmounts(10520.55, 0 , 10520.55 , 1000000 );
        jul.setAmounts(9863.01, 0 , 9863.01 , 1000000 );
        aug.setAmounts(10191.78, 0 , 10191.78 , 1000000 );
        sep.setAmounts(1010191.78, 1000000 , 10191.78 , 0);

        System.err.println(loan);

        checkIfEquals(jun, loan.scheduleItems.get(0));
        checkIfEquals(jul, loan.scheduleItems.get(1));
        checkIfEquals(aug, loan.scheduleItems.get(2));
        checkIfEquals(sep, loan.scheduleItems.get(3));
    }

    private void checkIfEquals(Repayment expected, ScheduleItem scheduleItem)
    {
        assertEquals(Repayment.class, scheduleItem.getClass());
        Repayment actual = (Repayment)scheduleItem;
        assertEquals(expected.amount.doubleValue(), actual.amount.doubleValue(), 0.001);
        assertEquals(expected.principal.doubleValue(), actual.principal.doubleValue(), 0.001);
        assertEquals(expected.interest.doubleValue(), actual.interest.doubleValue(), 0.001);
        assertEquals(expected.beginningBalance.doubleValue(), actual.beginningBalance.doubleValue(), 0.001);
        assertEquals(expected.dueDate, actual.dueDate);
    }


    @Test
    public void testSchedulingOrder_DoneEventsAreSortedByActualDate()
    {
        LocalDate now = LocalDate.of(2019, Month.JULY, 1);
        Repayment r1 = new Repayment(now.plusDays(3));
        r1.setActualDate(now.minusDays(3));
        Funding f2 = new Funding(now.plusDays(2), BigDecimal.ONE);
        f2.setActualDate(now.minusDays(2));
        Repayment r3 = new Repayment(now.plusDays(1));
        r3.setActualDate(now.minusDays(1));

        BusinessLoan.ScheduleItemComparator comp = new BusinessLoan.ScheduleItemComparator(now);
        assertOrder(comp, r1, f2, r3);

        comp = new BusinessLoan.ScheduleItemComparator(now.plusDays(10));
        assertOrder(comp, r1, f2, r3);

        comp = new BusinessLoan.ScheduleItemComparator(now.minusDays(10));
        assertOrder(comp, r1, f2, r3);
    }

    @Test
    public void testSchedulingOrder_DoneEventsAreSortedByActualDate_IncludingInterestChange()
    {
        LocalDate now = LocalDate.of(2019, Month.JULY, 1);
        Repayment r1 = new Repayment(now.plusDays(3));
        r1.setActualDate(now.minusDays(3));
        Funding f2 = new Funding(now.plusDays(2), BigDecimal.ONE);
        f2.setActualDate(now.minusDays(2));
        InterestChange i3 = new InterestChange(now.plusDays(1), BigDecimal.ONE);

        BusinessLoan.ScheduleItemComparator comp = new BusinessLoan.ScheduleItemComparator(now);
        assertOrder(comp, r1, f2, i3);

        comp = new BusinessLoan.ScheduleItemComparator(now.plusDays(10));
        assertOrder(comp, r1, f2, i3);

        comp = new BusinessLoan.ScheduleItemComparator(now.minusDays(10));
        assertOrder(comp, r1, f2, i3);
    }

    @Test
    public void testSchedulingOrder_DoneEventsOfSameActualDateAreSortedByDueDate()
    {
        LocalDate now = LocalDate.of(2019, Month.JULY, 1);
        Repayment r1 = new Repayment(now.plusDays(1));
        r1.setActualDate(now);
        Repayment r2 = new Repayment(now.plusDays(2), BigDecimal.ONE);
        r2.setActualDate(now);
        Repayment r3 = new Repayment(now.plusDays(3));
        r3.setActualDate(now);

        BusinessLoan.ScheduleItemComparator comp = new BusinessLoan.ScheduleItemComparator(now);
        assertOrder(comp, r1, r2, r3);

        comp = new BusinessLoan.ScheduleItemComparator(now.plusDays(10));
        assertOrder(comp, r1, r2, r3);

        comp = new BusinessLoan.ScheduleItemComparator(now.minusDays(10));
        assertOrder(comp, r1, r2, r3);
    }

    private void assertOrder(Comparator comp, ScheduleItem i1, ScheduleItem i2, ScheduleItem i3)
    {
        assertTrue(comp.compare(i1, i2) < 0);
        assertTrue(comp.compare(i2, i1) > 0);

        assertTrue(comp.compare(i3, i2) > 0);
        assertTrue(comp.compare(i2, i3) < 0);
    }

    @Test
    public void testSchedulingOrder_NotDoneEventsAreSortedByDueDate()
    {
        LocalDate now = LocalDate.of(2019, Month.JULY, 1);
        Repayment r1 = new Repayment(now.minusDays(1));
        Funding f2 = new Funding(now, BigDecimal.ONE);
        Repayment r3 = new Repayment(now.plusDays(1));

        //some overdue, other not
        BusinessLoan.ScheduleItemComparator comp = new BusinessLoan.ScheduleItemComparator(now);
        assertOrder(comp, r1, f2, r3);

        //none overdue
        comp = new BusinessLoan.ScheduleItemComparator(now.plusDays(10));
        assertOrder(comp, r1, f2, r3);

        //overdue
        comp = new BusinessLoan.ScheduleItemComparator(now.minusDays(10));
        assertOrder(comp, r1, f2, r3);
    }

    @Test
    public void testSchedulingOrder_DoneItemsBeforeNotDoneItems_NoInterestChange()
    {
        LocalDate now = LocalDate.of(2019, Month.JULY, 1);
        Repayment r1NotDone = new Repayment(now.minusDays(3));
        Funding f2Done = new Funding(now, BigDecimal.ONE);
        f2Done.actualDate = now;
        Funding f3NotDone = new Funding(now.minusDays(1), BigDecimal.ONE);

        //over due
        BusinessLoan.ScheduleItemComparator comp = new BusinessLoan.ScheduleItemComparator(now);
        assertOrder(comp, f2Done, r1NotDone, f3NotDone);

        //not overdue
        comp = new BusinessLoan.ScheduleItemComparator(now.plusDays(10));
        assertOrder(comp, f2Done, r1NotDone, f3NotDone);
    }

    @Test
    public void testSchedulingOrder_OverdueEventsSortedAfterCurrentInterestChange()
    {
        LocalDate now = LocalDate.of(2019, Month.JULY, 1);
        InterestChange interestChange = new InterestChange(now, BigDecimal.TEN);
        Repayment repayment = new Repayment(now.minusDays(3));
        Funding funding = new Funding(now.minusDays(2), BigDecimal.ONE);

        //no overdue, current interest
        BusinessLoan.ScheduleItemComparator comp = new BusinessLoan.ScheduleItemComparator(now.minusDays(5));
        assertOrder(comp, repayment, funding, interestChange);

        //all overdue
        comp = new BusinessLoan.ScheduleItemComparator(now);
        assertEquals(-1, comp.compare(repayment, funding));
        assertEquals(1, comp.compare(funding, repayment));

        //overdue, future interest
        InterestChange futureInterest = new InterestChange(now.plusDays(1), BigDecimal.ONE);
        assertOrder(comp, repayment, funding, futureInterest);

        //overdue, current interest
        assertOrder(comp, interestChange, repayment, funding);
    }
}