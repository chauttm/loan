import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * Created by qsoft on 5/17/17.
 */
public class Funding extends ScheduleItem
{
    public Funding(LocalDate dueDate, BigDecimal principal)
    {
        super(dueDate, principal);
    }

    void receive(LocalDate actualDate)
    {
        super.finish(actualDate);
    }

    @Override
    public BigDecimal getEndingBalance()
    {
        return beginningBalance.add(amount);
    }

    @Override
    public void calculate(BusinessLoan.InterestData interestData, LocalDate now, BusinessLoan loan)
    {
        LocalDate receiptDate = done ? actualDate : (dueDate.isBefore(now) ? now : dueDate);
        beginningBalance = interestData.balance;

        long days = ChronoUnit.DAYS.between(interestData.latestAccumulatedInterestDate, receiptDate) - 1; //not including receipt date
        BigDecimal interest = beginningBalance.multiply(BigDecimal.valueOf(days * interestData.dailyRate));
        interestData.accumulatedInterest = interestData.accumulatedInterest.add(interest);

        interestData.balance = getEndingBalance();
        interestData.latestAccumulatedInterestDate = receiptDate.minusDays(1);
    }

    public String toString()
    {
        return super.toString() + "\t\t\t\t\t\t\t" + getEndingBalance() + "\n";
    }
}
