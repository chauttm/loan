import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Repayment extends ScheduleItem
{
    BigDecimal principal = BigDecimal.ZERO;
    BigDecimal interest = BigDecimal.ZERO;
    boolean addedByUser = false;

    public Repayment(LocalDate dueDate)
    {
        super(dueDate, null);
    }

    public Repayment(LocalDate dueDate, BigDecimal amount)
    {
        super(dueDate, amount);
    }

    public Repayment(LocalDate dueDate, BigDecimal amount, BigDecimal principal, BigDecimal interest) {
        super(dueDate, amount);
        this.principal = principal;
        this.interest = interest;
    }

    public String toString()
    {
        return super.toString() + "\t"  + principal + "\t" + interest + "\t" + getEndingBalance() + "\n";
    }

    @Override
    public BigDecimal getEndingBalance()
    {
        return beginningBalance.subtract(principal);
    }

    @Override
    public void calculate(BusinessLoan.InterestData interestData, LocalDate now, BusinessLoan loan)
    {
        if (interestData.balance.compareTo(BigDecimal.ZERO) == 0) {
            if (amount.compareTo(BigDecimal.ZERO) != 0 && actualDate != null) {
                throw new RuntimeException("Error: loan paid off before a paid repayment on " + actualDate);
            }
            deleted = true;
            return;
        }
        LocalDate paidDate = actualDate;
        beginningBalance = interestData.balance;
        if (!done) {
            paidDate = dueDate.isBefore(now) ? now : dueDate;
            long days = ChronoUnit.DAYS.between(interestData.latestAccumulatedInterestDate, paidDate);
            interest = beginningBalance.multiply(BigDecimal.valueOf(days * interestData.dailyRate));
            interest = interest.add(interestData.accumulatedInterest).setScale(loan.scale, RoundingMode.HALF_UP);

            if (addedByUser) {
                principal = amount.subtract(interest);
            }
            else {
                switch (loan.repaymentType) {
                    case PpI:
                        principal = beginningBalance.compareTo(loan.fixedAmount) > 0 ? loan.fixedAmount : beginningBalance;
                        amount = principal.add(interest);
                        break;
                    case PnI:
                        if (amount == null || amount.compareTo(loan.fixedAmount) < 0) amount = loan.fixedAmount;
                        principal = amount.subtract(interest);
                        if (principal.compareTo(beginningBalance) > 0) {
                            principal = beginningBalance;
                            amount = principal.add(interest);
                        }
                        break;
                    case Int:
                        amount = principal.add(interest);
                        break;
                    default:
                        throw new RuntimeException("Not supported " + loan.repaymentType);
                }
            }
        }
        interestData.accumulatedInterest = BigDecimal.ZERO;
        interestData.balance = getEndingBalance();
        interestData.latestAccumulatedInterestDate = paidDate;
    }

    void pay()
    {
        pay(dueDate);
    }

    void pay(LocalDate actualDate)
    {
        super.finish(actualDate);
    }

    public void setAmounts(double payment, double principal, double interest, double balance) {
        this.amount = BigDecimal.valueOf(payment);
        this.principal = BigDecimal.valueOf(principal);
        this.interest = BigDecimal.valueOf(interest);
        this.beginningBalance = BigDecimal.valueOf(balance).add(this.principal);
    }

    public void pay(BigDecimal amount, LocalDate paymentDate)
    {
        this.amount = amount;
        principal = amount.subtract(interest);
        pay(paymentDate);
    }
}
