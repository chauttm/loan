import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Comparator;

abstract public class ScheduleItem
{
    protected BigDecimal amount; // always positive, principal in case of funding
    protected BigDecimal beginningBalance;
    protected LocalDate dueDate;
    protected LocalDate actualDate;
    protected boolean done = false;
    protected boolean deleted = false;

    public ScheduleItem(LocalDate dueDate, BigDecimal amount)
    {
        this.amount = amount;
        this.dueDate = dueDate;
    }

    public BigDecimal getAmount()
    {
        return amount;
    }

    public String toString()
    {
        return dueDate + ", " + actualDate + " - " + beginningBalance + "\t" + amount;
    }

    protected void finish(LocalDate actualDate)
    {
        done = true;
        this.actualDate = actualDate;
    }

    public abstract BigDecimal getEndingBalance();

    public abstract void calculate(BusinessLoan.InterestData interestData, LocalDate now, BusinessLoan loan);

    public LocalDate getDueDate()
    {
        return dueDate;
    }

    public LocalDate getActualDate() {
        return actualDate;
    }

    public void setActualDate(LocalDate actualDate) {
        this.actualDate = actualDate;
    }
}
