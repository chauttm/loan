import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.*;

public class BusinessLoan
{
    public static int SCALE = 2;
    public static final double DaysOfYear = 365;
    LocalDate firstFundingDate;
    LocalDate firstPaymentDate;
    BigDecimal principal;
    RepaymentType repaymentType;
    PaymentInterval paymentInterval = PaymentInterval.Monthly;
    int terms;
    BigDecimal fixedAmount; //principal in PrincipalPlusInterest or payment in PrincipalAndInterest.
    int scale;
    Map<LocalDate, BigDecimal> interestMap = new HashMap<>();
    List<Repayment> repayments = new ArrayList<>();
    List<Funding> fundings = new ArrayList<>();

    List<ScheduleItem> scheduleItems = new ArrayList<>();

    public BusinessLoan(LocalDate firstFundingDate, LocalDate firstPaymentDate, BigDecimal principal, RepaymentType type, int terms, BigDecimal interestRate)
    {
        this.firstFundingDate = firstFundingDate;
        this.firstPaymentDate = firstPaymentDate;
        this.principal = principal;
        this.repaymentType = type;
        this.terms = terms;
        interestMap.put(firstFundingDate, interestRate);
        scale = SCALE; //todo: localize
        generateRepayments();
        reconstructSchedule(firstFundingDate, true);
    }

    private void generateRepayments() {
        switch (repaymentType) {
            case PpI:
                fixedAmount = principal.divide(BigDecimal.valueOf(terms), scale, RoundingMode.HALF_UP);
                for (int term = 1; term <= terms; term++) {
                    repayments.add(new Repayment(calculateDueDate(term), null, fixedAmount, null));
                }
                break;
            case PnI:
                fixedAmount = fixedAmount!=null && fixedAmount.compareTo(BigDecimal.ZERO) > 0 ? fixedAmount
                        : BusinessLoanUtils.calculatePniFixedPayment(principal, terms, paymentInterval, interestMap.get(firstFundingDate), scale);
                for (int term = 1; term <= terms; term++) {
                    repayments.add(new Repayment(calculateDueDate(term), fixedAmount, null, null));
                }
                break;
            case Int:
                fixedAmount = BigDecimal.ZERO;
                for (int term = 1; term < terms; term++) {
                    repayments.add(new Repayment(calculateDueDate(term), null, BigDecimal.ZERO, null));
                }
                repayments.add(new Repayment(calculateDueDate(terms), null, principal, null));
                break;
            default:
                throw new RuntimeException("Not supported " + repaymentType);
        }
    }

    public BusinessLoan()
    {

    }

    public Repayment addUnpaidRepayment(LocalDate dueDate, BigDecimal amount, LocalDate now) {
        Repayment repayment = new Repayment(dueDate, amount);
        repayment.addedByUser = true;
        repayments.add(repayment);
        reconstructSchedule(now, false);
        return repayment;
    }

    public static class ScheduleItemComparator implements Comparator<ScheduleItem>
    {
        LocalDate now;
        ScheduleItemComparator(LocalDate now)
        {
            this.now = now;
        }
        @Override
        public int compare(ScheduleItem o1, ScheduleItem o2) {

            //at least one of the item is interest change.
            if (o1.getClass().equals(InterestChange.class)) {
                LocalDate o2Date;
                if (o2.actualDate != null) o2Date = o2.actualDate;
                else if (o2.dueDate.isBefore(now)) o2Date = now;
                else o2Date = o2.dueDate;
                int comp = o1.actualDate.compareTo(o2Date);
                return comp != 0 ? comp : -1;
            } else if (o2.getClass().equals(InterestChange.class)) {
                return -compare(o2, o1);
            }

            //both are repayment or funding

            //sort by date ascending, done first
            LocalDate o1Date = o1.actualDate == null ? o1.dueDate : o1.actualDate;
            LocalDate o2Date = o2.actualDate == null ? o2.dueDate : o2.actualDate;
            int comparison = o1Date.compareTo(o2Date);
            if (o1.getClass().equals(InterestChange.class) || o2.getClass().equals(InterestChange.class)) return comparison;

            if (o1.actualDate != null && o2.actualDate != null) comparison = o1.actualDate.compareTo(o2.actualDate);
            else if (o1.actualDate != null) return -1;
            else if (o2.actualDate != null) return 1;
            else comparison = o1.dueDate.compareTo(o2.dueDate);
            if (comparison != 0) return comparison;

            //then by class: Funding -> Repayment
            if (o1.getClass().equals(o2.getClass())) return o1.dueDate.compareTo(o2.dueDate);
            if (o1.getClass().equals(Funding.class)) return -1;
            if (o2.getClass().equals(Funding.class)) return 1;
            throw new RuntimeException("Should never get here");
        }
    }

    public void reconstructSchedule()
    {
        reconstructSchedule(firstFundingDate, false);
    }

    //call this function whenever a loan is reloaded from DB or viewed in UI
    public void reconstructSchedule(LocalDate now, boolean fitTerms)
    {
        scheduleItems = new ArrayList<>();
        repayments.stream().filter(r -> !r.deleted).forEach(repayment -> scheduleItems.add(repayment));
        fundings.stream().filter(f -> !f.deleted).forEach(funding -> scheduleItems.add(funding));
        interestMap.forEach((date, interest) -> {
            if (!date.equals(firstFundingDate)) scheduleItems.add(new InterestChange(date, interest));
        });
        ScheduleItemComparator comparator = new ScheduleItemComparator(now);
        scheduleItems.sort(comparator);
        calculateInterest(now, fitTerms);
    }

    public static double getDailyRate(BigDecimal annualPercentageRate)
    {
        return annualPercentageRate.doubleValue() / (DaysOfYear * 100);
    }

    private void calculateInterest(LocalDate now, boolean fitTerms)
    {
        InterestData interestData = new InterestData();
        interestData.dailyRate = getDailyRate(interestMap.get(firstFundingDate));
        interestData.latestAccumulatedInterestDate = firstFundingDate.minusDays(1);
        interestData.balance = principal;
        interestData.accumulatedInterest = BigDecimal.ZERO;

        Set<ScheduleItem> deleted = new HashSet<>();
        for (ScheduleItem scheduleItem : scheduleItems) {
            scheduleItem.calculate(interestData, now, this);
            if (scheduleItem.deleted) deleted.add(scheduleItem);
        }

        scheduleItems.removeAll(deleted);

        if (interestData.balance.compareTo(BigDecimal.ZERO) > 0) {
            if (fitTerms) {
                Repayment last = (Repayment) scheduleItems.get(scheduleItems.size()-1);
                last.principal = last.beginningBalance;
                last.amount = last.principal.add(last.interest);
            }
            else {
                int lastTerm = calculateLastTerm();
                while (interestData.balance.compareTo(BigDecimal.ZERO) > 0) {
                    Repayment addedRepayment = new Repayment(calculateDueDate(lastTerm++));
                    if (repaymentType == RepaymentType.Int) {
                        addedRepayment.principal = interestData.balance;
                    }
                    addedRepayment.calculate(interestData, now, this);
                    scheduleItems.add(addedRepayment);
                    repayments.add(addedRepayment);
                }
            }
        }
    }

    private int calculateLastTerm()
    {
        LocalDate maxDueDate = repayments.stream().filter(r -> !r.deleted)
                .map(ScheduleItem::getDueDate).max(Comparator.comparing(LocalDate::toEpochDay)).get();
        int term = 2;
        while (!calculateDueDate(term).isAfter(maxDueDate)) {
            term++;
        }
        return term;
    }

    public LocalDate calculateDueDate(int termNo)
    {
        switch (paymentInterval) {
            case Daily: return firstPaymentDate.plusDays(termNo - 1);
            case Weekly: return firstPaymentDate.plusWeeks(termNo - 1);
            case Monthly: return firstPaymentDate.plusMonths(termNo - 1);
            case Quarterly: return firstPaymentDate.plusMonths(3 * (termNo - 1));
            case Anually: return firstPaymentDate.plusYears(termNo - 1);
            default: throw new RuntimeException("payment interval not supported " + paymentInterval.toString());
        }
    }

    public void payAfterRecalculatingInterest(Repayment repayment, LocalDate actualDate, LocalDate now)
    {
        payAfterRecalculatingInterest(repayment, repayment.amount, actualDate, now);
    }

    public void payAfterRecalculatingInterest(Repayment repayment, BigDecimal amount, LocalDate paymentDate, LocalDate now)
    {
        reconstructSchedule(paymentDate, false); //recalculate interests as of now
        repayment.pay(amount, paymentDate);
        reconstructSchedule(now, false);
    }

    public void payWithoutRecalculatingInterest(Repayment repayment, LocalDate paymentDate, LocalDate now)
    {
        repayment.pay(paymentDate);
        reconstructSchedule(now, false);
    }

    public void payAfterRecalculatingInterest(Repayment repayment, BigDecimal amount)
    {
        payAfterRecalculatingInterest(repayment, amount, repayment.dueDate, repayment.dueDate);
    }

    public Funding addFunding(LocalDate date, BigDecimal principal, boolean received, LocalDate now)
    {
        Funding funding = new Funding(date, principal);
        funding.done = received;
        funding.actualDate = received ? date : null;
        fundings.add(funding);
        reconstructSchedule(now, false);
        return funding;
    }

    public void addInterestChange(LocalDate startDate, BigDecimal annualPercentageRate, LocalDate now)
    {
        interestMap.put(startDate, annualPercentageRate);
        reconstructSchedule(now, false);
    }

    public String toString()
    {
        StringBuffer buffer = new StringBuffer();
        buffer.append("First funding date: " + firstFundingDate + "\n");
        buffer.append(scheduleItems);
        return buffer.toString();
    }

    public static class InterestData
    {
        double dailyRate;
        LocalDate latestAccumulatedInterestDate;
        BigDecimal balance;
        BigDecimal accumulatedInterest;

        public String toString()
        {
            return "[" + dailyRate + "\t" + balance + "\t" + accumulatedInterest + "\t" + latestAccumulatedInterestDate + "]";
        }
    }

    public enum PaymentInterval {
        Anually, BiAnnually, Quarterly, Monthly, Weekly, Daily;
    }

    public enum RepaymentType
    {
        PnI,    // Principal & Interest
        PpI,    // Principal plus Interest
        Int;    // Interest only
    }
}
