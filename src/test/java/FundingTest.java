import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;

import static org.junit.Assert.*;

/**
 * Created by qsoft on 5/17/17.
 */
public class FundingTest
{
    BusinessLoan loan = new BusinessLoan();
    BusinessLoan.InterestData interestData = new BusinessLoan.InterestData();

    @Before
    public void setup()
    {
        loan.fixedAmount = BigDecimal.valueOf(100000); // don't care
        interestData.balance = BigDecimal.valueOf(1000);
        interestData.dailyRate = 0.001;
        interestData.accumulatedInterest = BigDecimal.ONE;
    }

    @Test
    public void testCalculateInterest_ReceivesByNow()
    {
        LocalDate dueDate = LocalDate.of(2016, Month.JULY, 10);
        interestData.latestAccumulatedInterestDate = LocalDate.of(2016, Month.JUNE, 30);
        LocalDate now = LocalDate.of(2016, Month.JULY, 10);
        Funding funding = new Funding(dueDate, BigDecimal.valueOf(1000));

        funding.calculate(interestData, now, loan);

        assertEquals("jul01 - Jul09", 9 + 1, interestData.accumulatedInterest.doubleValue(), 0.01) ;
    }

    @Test
    public void testCalculateInterest_KeepWhenLoanHasBeenPaidOff()
    {
        LocalDate dueDate = LocalDate.of(2016, Month.JULY, 10);
        interestData.balance = BigDecimal.ZERO;
        interestData.latestAccumulatedInterestDate = LocalDate.of(2016, Month.JUNE, 1);
        Funding funding = new Funding(dueDate, BigDecimal.valueOf(1000));
        funding.calculate(interestData, dueDate, loan);

        assertFalse("should not be deleted", funding.deleted);
        assertEquals(1000, interestData.balance.doubleValue(), 0.01);
    }
}
