import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RepaymentTest
{
    BusinessLoan loan = new BusinessLoan();
    BusinessLoan.InterestData interestData = new BusinessLoan.InterestData();

    @Before
    public void setup()
    {
        loan.fixedAmount = BigDecimal.valueOf(100000); // don't care
        loan.repaymentType = BusinessLoan.RepaymentType.PnI;
        interestData.balance = BigDecimal.valueOf(1000);
        interestData.dailyRate = 0.001;
        interestData.accumulatedInterest = BigDecimal.ZERO;
    }

    @Test
    public void testCalculateInterest_PaymentNotOverDue()
    {
        LocalDate dueDate = LocalDate.of(2016, Month.JULY, 10);
        interestData.latestAccumulatedInterestDate = LocalDate.of(2016, Month.JUNE, 30);
        LocalDate now = LocalDate.of(2016, Month.JULY, 3);
        loan.fixedAmount = BigDecimal.valueOf(500);
        Repayment repayment = new Repayment(dueDate, loan.fixedAmount);
        assertEquals("amount", 500, repayment.amount.doubleValue(), 0.001);

        repayment.calculate(interestData, now, loan);
        assertEquals("no accumulated interest", 0, interestData.accumulatedInterest.doubleValue(), 0.01) ;
        assertEquals("interest to payAfterRecalculatingInterest", 10, repayment.interest.doubleValue(), 0.001);
        assertEquals("amount", 500, repayment.amount.doubleValue(), 0.001);
        assertEquals("principal", 500 - repayment.interest.doubleValue(), repayment.principal.doubleValue(), 0.001);
    }

    @Test
    public void testCalculateInterest_PnI_PaymentNotOverDue()
    {
        loan.fixedAmount = BigDecimal.valueOf(256281.00);
        LocalDate dueDate = LocalDate.of(2017, Month.JUNE, 21);
        interestData.latestAccumulatedInterestDate = LocalDate.of(2017, Month.MAY, 20);
        interestData.balance = BigDecimal.valueOf(1000000.00);
        interestData.dailyRate = BusinessLoan.getDailyRate(BigDecimal.valueOf(12));
        LocalDate now = dueDate;
        Repayment repayment = new Repayment(dueDate, loan.fixedAmount);
        assertEquals("amount", loan.fixedAmount.doubleValue(), repayment.amount.doubleValue(), 0.001);

        repayment.calculate(interestData, now, loan);
        assertEquals("accumulated interest", 0, interestData.accumulatedInterest.doubleValue(), 0.001) ;
        assertEquals("interest to payAfterRecalculatingInterest", 10521.00, repayment.interest.doubleValue(), 0.001);
        checkValues(repayment, loan.fixedAmount.doubleValue());
    }

    @Test
    public void testCalculateInterest_PnI_PaidBeforeDue()
    {
        loan.fixedAmount = BigDecimal.valueOf(256281.00);
        LocalDate dueDate = LocalDate.of(2017, Month.JUNE, 21);
        interestData.latestAccumulatedInterestDate = LocalDate.of(2017, Month.MAY, 20);
        interestData.balance = BigDecimal.valueOf(1000000.00);
        interestData.dailyRate = BusinessLoan.getDailyRate(BigDecimal.valueOf(12));

        Repayment repayment = new Repayment(dueDate, loan.fixedAmount);
        assertEquals("amount", loan.fixedAmount.doubleValue(), repayment.amount.doubleValue(), 0.001);
        LocalDate paidDate = dueDate.minusDays(10);
        repayment.pay(paidDate);
        repayment.calculate(interestData, paidDate, loan);
        assertEquals(paidDate, interestData.latestAccumulatedInterestDate);
    }

    public void checkValues(Repayment repayment, double amount)
    {
        assertEquals("amount", amount, repayment.amount.doubleValue(), 0.001);
        assertEquals("principal", amount - repayment.interest.doubleValue(), repayment.principal.doubleValue(), 0.001);
    }

    @Test
    public void testCalculateInterest_ShouldIncludeAccumulatedInterest()
    {
        LocalDate dueDate = LocalDate.of(2016, Month.JULY, 10);
        interestData.latestAccumulatedInterestDate = LocalDate.of(2016, Month.JUNE, 30);
        LocalDate now = LocalDate.of(2016, Month.JULY, 3);
        loan.fixedAmount = BigDecimal.valueOf(1000);
        Repayment repayment = new Repayment(dueDate, loan.fixedAmount);
        repayment.addedByUser = false;
        double accumulatedInterest = 6;
        interestData.accumulatedInterest = BigDecimal.valueOf(accumulatedInterest);

        repayment.calculate(interestData, now, loan);

        assertEquals("no accumulated interest", 0, interestData.accumulatedInterest.doubleValue(), 0.01) ;
        assertEquals("interest to payAfterRecalculatingInterest", 10 + accumulatedInterest, repayment.interest.doubleValue(), 0.001);

        checkValues(repayment, 1000);
    }

    @Test
    public void testCalculateInterest_PaymentOverDue()
    {
        LocalDate dueDate = LocalDate.of(2016, Month.JULY, 10);
        interestData.latestAccumulatedInterestDate = LocalDate.of(2016, Month.JUNE, 30);
        LocalDate now = LocalDate.of(2016, Month.JULY, 30);
        loan.fixedAmount = BigDecimal.valueOf(1000);
        Repayment repayment = new Repayment(dueDate, loan.fixedAmount);

        repayment.calculate(interestData, now, loan);

        assertEquals("no accumulated interest", 0, interestData.accumulatedInterest.doubleValue(), 0.01) ;
        assertEquals("interest to payAfterRecalculatingInterest", 30, repayment.interest.doubleValue(), 0.001);

        checkValues(repayment, 1000);
    }

    @Test
    public void testCalculateInterest_PaymentOverDueAndLastPayDateIsNow()
    {
        LocalDate dueDate = LocalDate.of(2016, Month.JULY, 10);
        LocalDate now = LocalDate.of(2016, Month.JULY, 30);
        interestData.latestAccumulatedInterestDate = now;
        Repayment repayment = new Repayment(dueDate, BigDecimal.valueOf(1000));
        repayment.calculate(interestData, now, loan);

        assertEquals("no accumulated interest", 0, interestData.accumulatedInterest.doubleValue(), 0.01) ;
        assertEquals("interest to payAfterRecalculatingInterest", 0, repayment.interest.doubleValue(), 0.001);

        checkValues(repayment, 1000);
    }

    @Test
    public void testCalculateInterest_DeleteWhenLoanHasBeenPaidOff()
    {
        LocalDate dueDate = LocalDate.of(2016, Month.JULY, 10);
        interestData.balance = BigDecimal.ZERO;
        Repayment repayment = new Repayment(dueDate, BigDecimal.valueOf(1000));
        repayment.calculate(interestData, dueDate, loan);

        assertTrue("deleted", repayment.deleted);
    }

    @Test
    public void testCalculateInterest_PaymentNotChangeIfPaid()
    {
        LocalDate dueDate = LocalDate.of(2016, Month.JULY, 10);
        LocalDate lastPayDate = LocalDate.of(2016, Month.JUNE, 30);
        LocalDate now = LocalDate.of(2016, Month.JULY, 30);
        loan.fixedAmount = BigDecimal.valueOf(1000);
        Repayment repayment = new Repayment(dueDate, loan.fixedAmount);

        interestData.latestAccumulatedInterestDate = lastPayDate;
        interestData.balance = BigDecimal.valueOf(3000);
        repayment.calculate(interestData, lastPayDate, loan);

        BigDecimal oldInterest = repayment.interest;
        repayment.pay();

        repayment.calculate(interestData, now, loan);
        assertEquals("no accumulated interest", 0, interestData.accumulatedInterest.doubleValue(), 0.01) ;
        assertEquals("interest to payAfterRecalculatingInterest", oldInterest.doubleValue(), repayment.interest.doubleValue(), 0.001);

        checkValues(repayment, 1000);
    }

    @Test
    public void testCalculateInterest_PaymentChangesIfGeneratedAndLessThanFixedAmount()
    {
        LocalDate dueDate = LocalDate.of(2016, Month.JULY, 10);
        LocalDate lastPayDate = LocalDate.of(2016, Month.JUNE, 30);
        LocalDate now = LocalDate.of(2016, Month.JULY, 30);
        loan.fixedAmount = BigDecimal.valueOf(1500);
        loan.repaymentType = BusinessLoan.RepaymentType.PnI;
        Repayment repayment = new Repayment(dueDate, BigDecimal.valueOf(1000));
        repayment.addedByUser = false;

        interestData.latestAccumulatedInterestDate = lastPayDate;
        interestData.balance = BigDecimal.valueOf(2000);
        repayment.calculate(interestData, lastPayDate, loan);

        checkValues(repayment, 1500);
    }

    @Test
    public void testCalculateInterest_PaymentNotChangesIfAddedByUser()
    {
        LocalDate dueDate = LocalDate.of(2016, Month.JULY, 10);
        LocalDate lastPayDate = LocalDate.of(2016, Month.JUNE, 30);
        LocalDate now = LocalDate.of(2016, Month.JULY, 30);
        loan.fixedAmount = BigDecimal.valueOf(1500);
        loan.repaymentType = BusinessLoan.RepaymentType.PnI;
        Repayment repayment = new Repayment(dueDate, BigDecimal.valueOf(1000));
        repayment.addedByUser = true;

        interestData.latestAccumulatedInterestDate = lastPayDate;
        interestData.balance = BigDecimal.valueOf(2000);
        repayment.calculate(interestData, lastPayDate, loan);

        checkValues(repayment, 1000);
    }
}
